import {
    types,
} from "mobx-state-tree";

const nameModel = types.model({
    isLoggedIn: types.optional(types.boolean, false),
    name: types.maybeNull(types.string),
}).actions(function nameModelActions(self) {
    function changeName(newName) {
        self.name = newName;
    }

    return {
        changeName,
    };
});

export default nameModel;