import {
    types,
} from "mobx-state-tree";
import userModel from "./user";


export default types.model({
    user: userModel
});