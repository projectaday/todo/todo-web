import defaultState from "./defaultState";
import model from "./models/index";

export function createModel(initialState = defaultState) {
    return model.create(initialState);
}

export default {
    createModel,
};