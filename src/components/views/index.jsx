import React, { Component } from 'react';
import Dashboard from "./main";
import { Switch, Route } from "react-router-dom";

const Index = () => <h2>LOGIN</h2>;

class ViewWrapper extends Component {
  render() {
    return (
        <Switch>
            <Route path="/login" exact component={Index} />
            <Route component={Dashboard} />
        </Switch>
    );
  }
}


export default ViewWrapper;
