import home from "./home";
import about from "./about";

export default [
    {
        name: "Home",
        route: "/",
        exact: true,
        component: home,
        
    },
    {
        name: "About",
        route: "/about",
        component: about,
        
    }
];