import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import ViewWrapper from "./views/index";
import { BrowserRouter as Router, } from "react-router-dom";
class App extends Component {
  render() {
    return (
      <Router>
        <ViewWrapper></ViewWrapper>
      </Router>
    );
  }
}

const expObj = inject("appStore")(observer(App));
expObj[Symbol.for("Component")] = App;
export default expObj;