import React from 'react';
import ReactDOM from 'react-dom';
import {
    Provider
} from "mobx-react";
import App from './components/App';
import {
    createModel,
} from "./state/index";
import { connectReduxDevtools } from "mst-middlewares"


const appStore = createModel({
    user: {
        name: "Jonah"
    }
});

connectReduxDevtools(require("remotedev"), appStore);

ReactDOM.render(
    <Provider appStore={appStore}>
        <App />
    </Provider>
    , document.getElementById('root'));


